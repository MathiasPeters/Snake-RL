from enum import Enum

from player.agents.hamilton import Hamilton
from player.agents.random_agent import RandomAgent
from player.agents.shortest_path_agent import ShortestPathAgent
from player.human_player import HumanPlayer
from player.player import Player


class PlayerType(Enum):
    HUMAN = 0
    RANDOM = 1
    SHORTEST_PATH = 2
    HAMILTON = 3


def get_player(player_type: PlayerType = PlayerType.HAMILTON) -> Player:
    if player_type == PlayerType.HUMAN:
        return HumanPlayer()
    elif player_type == PlayerType.RANDOM:
        return RandomAgent()
    elif player_type == PlayerType.SHORTEST_PATH:
        return ShortestPathAgent()
    else:
        return Hamilton()
