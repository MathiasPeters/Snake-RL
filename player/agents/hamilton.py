from config.game_config import GAME_CONFIG
from player.direction import Direction
from player.player import Player
from rendering.canvas import Pos


class Hamilton(Player):
    def process_environment(self, food: Pos) -> None:
        self._has_made_a_move = False

        top = left = 0
        bottom = right = GAME_CONFIG.game_size - 1

        head_x = self.body[0][0]
        head_y = self.body[0][1]

        is_top_row = head_y == top
        is_first_column = head_x == left
        is_last_column = head_x == right

        is_second_row = head_y == (top + 1)
        is_bottom_row = head_y == bottom
        is_even_column = head_x % 2 == 0
        is_odd_column = not is_even_column

        if is_bottom_row and is_even_column:
            self.set_direction(Direction.RIGHT)
        elif is_bottom_row and is_odd_column:
            self.set_direction(Direction.UP)
        elif is_first_column:
            self.set_direction(Direction.DOWN)
        elif is_top_row:
            self.set_direction(Direction.LEFT)
        elif is_last_column:
            self.set_direction(Direction.UP)
        elif is_second_row and is_even_column and not is_first_column:
            self.set_direction(Direction.DOWN)
        elif is_second_row and is_odd_column and not is_last_column:
            self.set_direction(Direction.RIGHT)

        self._has_made_a_move = True
