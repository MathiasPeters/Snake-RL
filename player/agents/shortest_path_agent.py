from typing import List, Tuple

from config.game_config import GAME_CONFIG
from player.direction import Direction
from player.player import Player
from rendering.canvas import Pos


def contains(pos: Pos, list: List[Pos]) -> bool:
    for i in list:
        if pos[0] == i[0] and pos[1] == i[1]:
            return True
    return False


def move_is_valid(move: Pos, snake: List[Pos], taken: List[Pos]) -> bool:
    bound = GAME_CONFIG.game_size - 1

    if move[0] < 0 or move[0] > bound:
        return False
    if move[1] < 0 or move[1] > bound:
        return False
    if contains(move, snake):
        return False
    if contains(move, taken):
        return False

    return True


def get_left_forward_and_right(current: Pos, direction: Direction) -> Tuple[Pos, Pos, Pos]:
    x, y = current
    if direction == Direction.UP:
        left = (x - 1, y)
        forward = (x, y - 1)
        right = (x + 1, y)
        return (left, forward, right)
    elif direction == Direction.DOWN:
        left = (x + 1, y)
        forward = (x, y + 1)
        right = (x - 1, y)
        return (left, forward, right)
    elif direction == Direction.LEFT:
        left = (x, y + 1)
        forward = (x - 1, y)
        right = (x, y - 1)
        return (left, forward, right)
    else:
        left = (x, y - 1)
        forward = (x + 1, y)
        right = (x, y + 1)
        return (left, forward, right)


def get_left_of(direction: Direction) -> Direction:
    if direction == Direction.UP:
        return Direction.LEFT
    elif direction == Direction.LEFT:
        return Direction.DOWN
    elif direction == Direction.DOWN:
        return Direction.RIGHT
    else:
        return Direction.UP


def get_right_of(direction: Direction) -> Direction:
    if direction == Direction.UP:
        return Direction.RIGHT
    elif direction == Direction.RIGHT:
        return Direction.DOWN
    elif direction == Direction.DOWN:
        return Direction.LEFT
    else:
        return Direction.UP


def food_is_straight_ahead(head: Pos, direction: Direction, food: Pos) -> bool:
    up_and_ahead = direction == Direction.UP and food[1] < head[1]
    down_and_ahead = direction == Direction.DOWN and food[1] > head[1]
    left_and_ahead = direction == Direction.LEFT and food[0] < head[0]
    right_and_ahead = direction == Direction.RIGHT and food[0] > head[0]

    return up_and_ahead or down_and_ahead or left_and_ahead or right_and_ahead


class ShortestPathAgent(Player):
    def process_environment(self, food: Pos) -> None:
        self._has_made_a_move = False

        action = None

        if food_is_straight_ahead(self.body[0], self.get_direction(), food):
            action = 1

        visited: List[Pos] = []

        left: List[Tuple[Pos, Direction]] = []
        forward: List[Tuple[Pos, Direction]] = []
        right: List[Tuple[Pos, Direction]] = []

        l, f, r = get_left_forward_and_right(self.body[0], self.get_direction())

        if move_is_valid(l, self.body, visited):
            left = [(l, get_left_of(self.get_direction()))]
            visited.append(l)

        if move_is_valid(f, self.body, visited):
            forward = [(f, self.get_direction())]
            visited.append(f)

        if move_is_valid(r, self.body, visited):
            right = [(r, get_right_of(self.get_direction()))]
            visited.append(r)

        while len(left) > 0 or len(forward) > 0 or len(right) > 0:
            go_left, new_left = self.process(left, visited, food)
            if go_left:
                action = 0
                break
            for pos, _ in new_left:
                visited.append(pos)
            left = new_left

            go_forward, new_forward = self.process(forward, visited, food)
            if go_forward:
                action = 1
                break
            for pos, _ in new_forward:
                visited.append(pos)
            forward = new_forward

            go_right, new_right = self.process(right, visited, food)
            if go_right:
                action = 2
                break
            for pos, _ in new_right:
                visited.append(pos)
            right = new_right

        if action == 0:
            self.__turn_left()
        elif action == 2:
            self.__turn_right()

        self._has_made_a_move = True

    def process(
        self, nodes: List[Tuple[Pos, Direction]], visited: List[Pos], food: Pos
    ) -> Tuple[bool, List[Tuple[Pos, Direction]]]:
        out_list: List[Tuple[Pos, Direction]] = []
        for pos, direction in nodes:
            if pos[0] == food[0] and pos[1] == food[1]:
                return (True, [])
            le, fo, ri = get_left_forward_and_right(pos, direction)
            if move_is_valid(le, self.body, visited):
                out_list.append((le, get_left_of(direction)))
                visited.append(le)

            if move_is_valid(fo, self.body, visited):
                out_list.append((fo, direction))
                visited.append(fo)

            if move_is_valid(ri, self.body, visited):
                out_list.append((ri, get_right_of(direction)))
                visited.append(ri)
        return (False, out_list)

    def __turn_left(self) -> None:
        new_direction = get_left_of(self.get_direction())
        self.set_direction(new_direction)

    def __turn_right(self) -> None:
        new_direction = get_right_of(self.get_direction())
        self.set_direction(new_direction)
