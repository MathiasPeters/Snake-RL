import random

from player.direction import Direction
from player.player import Player
from rendering.canvas import Pos


class RandomAgent(Player):
    def process_environment(self, food: Pos) -> None:
        self._has_made_a_move = False
        random.seed()
        action = random.randint(0, 2)
        if action == 0:
            self.__turn_left()
        elif action == 1:
            self.__turn_right()
        self._has_made_a_move = True

    def __turn_left(self) -> None:
        if self.get_direction() == Direction.UP:
            self.set_direction(Direction.LEFT)
        elif self.get_direction() == Direction.LEFT:
            self.set_direction(Direction.DOWN)
        elif self.get_direction() == Direction.DOWN:
            self.set_direction(Direction.RIGHT)
        elif self.get_direction() == Direction.RIGHT:
            self.set_direction(Direction.UP)

    def __turn_right(self) -> None:
        if self.get_direction() == Direction.UP:
            self.set_direction(Direction.RIGHT)
        elif self.get_direction() == Direction.RIGHT:
            self.set_direction(Direction.DOWN)
        elif self.get_direction() == Direction.DOWN:
            self.set_direction(Direction.LEFT)
        elif self.get_direction() == Direction.LEFT:
            self.set_direction(Direction.UP)
