import math
from typing import List

from config.game_config import GAME_CONFIG
from game.food import Food
from player.direction import Direction
from rendering.canvas import Pos


class Player:
    def __init__(self):
        self.body: List[Pos] = []
        self.direction: Direction = Direction.RIGHT
        self.out_of_bounds: bool = False
        self.collided_with_self: bool = False
        self._has_made_a_move = True

        self.reset()

    def get_direction(self) -> Direction:
        return self.direction

    def set_direction(self, direction: Direction) -> None:
        self.direction = direction

    def on_up_clicked(self) -> None:
        pass

    def on_down_clicked(self) -> None:
        pass

    def on_left_clicked(self) -> None:
        pass

    def on_right_clicked(self) -> None:
        pass

    def process_environment(self, food: Pos) -> None:
        pass

    def has_made_a_move(self) -> bool:
        return self._has_made_a_move

    def update(self, food: Food) -> None:
        new_head_x = self.body[0][0]
        new_head_y = self.body[0][1]
        if self.direction == Direction.UP:
            new_head_y -= 1
        elif self.direction == Direction.DOWN:
            new_head_y += 1
        elif self.direction == Direction.LEFT:
            new_head_x -= 1
        else:
            new_head_x += 1
        head = (new_head_x, new_head_y)
        self.body.insert(0, head)

        # Did eat?
        if head[0] == food.food[0] and head[1] == food.food[1]:
            food.food = None
        else:
            self.body.pop()

        # Out of bounds?
        out_x = head[0] < 0 or head[0] >= GAME_CONFIG.game_size
        out_y = head[1] < 0 or head[1] >= GAME_CONFIG.game_size
        if out_x or out_y:
            self.out_of_bounds = True

        # Self collision
        for i in range(1, len(self.body)):
            part = self.body[i]
            if head[0] == part[0] and head[1] == part[1]:
                self.collided_with_self = True

    def reset(self) -> None:
        middle = math.ceil(GAME_CONFIG.game_size / 2)
        self.body = [(middle, middle), (middle - 1, middle), (middle - 2, middle)]
        self.direction = Direction.RIGHT
        self.out_of_bounds = False
        self.collided_with_self = False
