from player.direction import Direction
from player.player import Player


class HumanPlayer(Player):
    def on_up_clicked(self) -> None:
        if self.get_direction() is not Direction.DOWN:
            self.set_direction(Direction.UP)

    def on_down_clicked(self) -> None:
        if self.get_direction() is not Direction.UP:
            self.set_direction(Direction.DOWN)

    def on_left_clicked(self) -> None:
        if self.get_direction() is not Direction.RIGHT:
            self.set_direction(Direction.LEFT)

    def on_right_clicked(self) -> None:
        if self.get_direction() is not Direction.LEFT:
            self.set_direction(Direction.RIGHT)

    def has_made_a_move(self) -> bool:
        return True
