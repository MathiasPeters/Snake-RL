import math

import pygame

from config.game_config import GAME_CONFIG


class Timer:
    def __init__(self):
        self.game_time = 0
        self.frame_time = 0

        self.frame_threshold = GAME_CONFIG.speed
        self.last_timestamp = pygame.time.get_ticks()

    def update(self) -> None:
        now = pygame.time.get_ticks()
        delta = now - self.last_timestamp

        self.game_time += delta
        self.frame_time += delta

        self.last_timestamp = pygame.time.get_ticks()

    def on_frame_updated(self) -> None:
        self.frame_time = 0

    def should_update_frame(self) -> bool:
        if self.frame_time >= self.frame_threshold:
            return True
        else:
            return False

    def get_pretty_time(self) -> str:
        time = math.floor(self.game_time / 1000)

        minutes = time // 60
        minutes_as_str = str(minutes).rjust(2, "0")

        seconds = time % 60
        seconds_as_str = str(seconds).rjust(2, "0")

        return minutes_as_str + ":" + seconds_as_str

    def reset(self) -> None:
        self.game_time = 0
        self.frame_time = 0
        self.last_timestamp = pygame.time.get_ticks()
