from typing import List

from config.app_config import APP_CONFIG
from config.game_config import GAME_CONFIG
from rendering.canvas import Alignment, Canvas, Pos
from rendering.colors import Color


class GameBoard:
    def __init__(self):
        self.box_size = 40
        self.menu_height = 50
        self.board_size = GAME_CONFIG.game_size * self.box_size + 1

        width = self.board_size
        height = self.board_size + self.menu_height

        self.canvas = Canvas(width, height)

    def __fill_grid(self, snake: List[Pos], food: Pos) -> None:
        grid = self.__get_grid(snake, food)
        draw_size = self.box_size - 1

        for i in range(len(grid)):
            y_pos = (i * self.box_size) + 1 + self.menu_height
            for j in range(len(grid[i])):
                x_pos = (j * self.box_size) + 1
                self.canvas.draw_rect((x_pos, y_pos, draw_size, draw_size), grid[j][i])

    def __get_grid(self, snake: List[Pos], food: Pos) -> List[List[Color]]:
        output = []

        # Clear all squares
        for _ in range(GAME_CONFIG.game_size):
            row = []
            for _ in range(GAME_CONFIG.game_size):
                row.append(APP_CONFIG.get_background_color())
            output.append(row)

        # Add snake
        snake_color = APP_CONFIG.get_snake_color()
        for pos in snake:
            x_pos = pos[0]
            y_pos = pos[1]
            output[x_pos][y_pos] = snake_color

        # Add food
        if food is not None:
            food_color = APP_CONFIG.get_food_color()
            x_pos = food[0]
            y_pos = food[1]
            output[x_pos][y_pos] = food_color

        return output

    def draw(self, time: str, score: int, snake: List[Pos], food: Pos) -> None:
        self.canvas.clear()

        self.canvas.draw_text(time, (0, 0), Alignment.LEFT, Alignment.TOP)
        self.canvas.draw_text(str(score), (0, 0), Alignment.RIGHT, Alignment.TOP)

        rect = (0, self.menu_height, self.board_size, self.board_size)
        self.canvas.draw_rect(rect, APP_CONFIG.get_grid_color())
        self.__fill_grid(snake, food)

        self.canvas.blit()

    def draw_game_over(self) -> None:
        self.canvas.draw_text("Game over", (0, 0), Alignment.CENTER, Alignment.CENTER)
        self.canvas.blit()
