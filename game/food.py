import random
from typing import List

from config.game_config import GAME_CONFIG
from rendering.canvas import Pos


class Food:
    def __init__(self, snake: List[Pos]):
        random.seed()
        self.bound = GAME_CONFIG.game_size - 1
        self.food = None
        self.get_new_food(snake)

    def get_new_food(self, snake: List[Pos]) -> None:
        new_food = self.__generate_food(snake)
        while self.__pos_is_taken(new_food, snake):
            new_food = self.__generate_food(snake)
        self.food = new_food

    def __generate_food(self, snake: List[Pos]) -> Pos:
        x_pos = random.randint(0, self.bound)
        y_pos = random.randint(0, self.bound)
        return (x_pos, y_pos)

    def __pos_is_taken(self, pos: Pos, snake: List[Pos]) -> bool:
        for i in snake:
            if i[0] == pos[0] and i[1] == pos[1]:
                return True
        return False
