import menu.main_menu
from event_handling.event_loop import EventLoop
from event_handling.frame_event import FrameEvent
from event_handling.key_event import KeyEvent
from game.food import Food
from game.game_board import GameBoard
from game.timer import Timer
from player.get_player import get_player


class Game:
    def __init__(self):
        self.game_board = GameBoard()
        self.event_loop = EventLoop()
        self.timer = Timer()
        self.player = get_player()
        self.food = Food(self.player.body)
        self.score = 0
        self.game_over = False
        self.__setup_events()

    def __update(self):
        if not self.game_over:
            self.timer.update()

            if self.timer.should_update_frame() and self.player.has_made_a_move():
                self.player.update(self.food)
                if self.player.out_of_bounds or self.player.collided_with_self:
                    self.game_over = True
                    self.game_board.draw_game_over()
                else:
                    self.game_board.draw(
                        self.timer.get_pretty_time(), self.score, self.player.body, self.food.food,
                    )

                self.timer.on_frame_updated()

                if self.food.food is None:
                    self.score += 1
                    self.food.get_new_food(self.player.body)

                self.player.process_environment(self.food.food)

    def __on_frame_started(self) -> None:
        pass

    def __go_to_main_menu(self) -> None:
        menu.main_menu.MainMenu()

    def __restart(self) -> None:
        if self.game_over:
            self.timer.reset()
            self.food.get_new_food(self.player.body)
            self.player.reset()
            self.score = 0
            self.game_over = False

    def __setup_events(self):
        self.event_loop.register_action(FrameEvent.FRAME_STARTED, self.__on_frame_started)
        self.event_loop.register_action(FrameEvent.FRAME_ENDED, self.__update)
        self.event_loop.register_action(KeyEvent.UP, self.player.on_up_clicked)
        self.event_loop.register_action(KeyEvent.DOWN, self.player.on_down_clicked)
        self.event_loop.register_action(KeyEvent.LEFT, self.player.on_left_clicked)
        self.event_loop.register_action(KeyEvent.RIGHT, self.player.on_right_clicked)
        self.event_loop.register_action(KeyEvent.ESCAPE, self.__go_to_main_menu)
        self.event_loop.register_action(KeyEvent.ENTER, self.__restart)
        self.event_loop.register_action(KeyEvent.SPACE, self.__restart)

        self.event_loop.run()
