from rendering.colors import BLACK, WHITE, Color, ColorMode, rgb


class AppConfig:
    def __init__(self):
        self.color_mode = ColorMode.LIGHT

    def get_background_color(self) -> Color:
        if self.color_mode == ColorMode.DARK:
            return BLACK
        else:
            return WHITE

    def get_text_color(self) -> Color:
        if self.color_mode == ColorMode.DARK:
            return WHITE
        else:
            return BLACK

    def get_snake_color(self) -> Color:
        if self.color_mode == ColorMode.DARK:
            return rgb(0, 255, 0)
        else:
            return BLACK

    def get_food_color(self) -> Color:
        if self.color_mode == ColorMode.DARK:
            return rgb(255, 0, 0)
        else:
            return rgb(0, 255, 0)

    def get_grid_color(self) -> Color:
        if self.color_mode == ColorMode.DARK:
            return WHITE
        else:
            return rgb(200, 200, 200)


APP_CONFIG = AppConfig()
