from enum import Enum


class GameType(Enum):
    HUMAN = 0
    AI = 1


class GameConfig:
    def __init__(self):
        self.speed = 150  # ms per frame
        self.game_size = 10
        self.game_type = GameType.HUMAN


GAME_CONFIG = GameConfig()
