from enum import Enum


class KeyEvent(Enum):
    UP = 0
    DOWN = 1
    LEFT = 2
    RIGHT = 3
    ESCAPE = 4
    ENTER = 5
    SPACE = 6
