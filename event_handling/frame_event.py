from enum import Enum


class FrameEvent(Enum):
    FRAME_STARTED = 0
    FRAME_ENDED = 1
