import sys

import pygame
from pygame.locals import *

from event_handling.frame_event import FrameEvent
from event_handling.key_event import KeyEvent

UPDATE = "update"


class EventLoop:
    def __init__(self):
        self.actions = {
            FrameEvent.FRAME_STARTED: lambda: None,
            FrameEvent.FRAME_ENDED: lambda: None,
            KeyEvent.UP: lambda: False,
            KeyEvent.DOWN: lambda: False,
            KeyEvent.LEFT: lambda: False,
            KeyEvent.RIGHT: lambda: False,
            KeyEvent.ESCAPE: lambda: False,
            KeyEvent.ENTER: lambda: False,
            KeyEvent.SPACE: lambda: False,
        }

    def register_action(self, key: KeyEvent, action) -> None:
        self.actions[key] = action

    def exit(self) -> None:
        pygame.quit()
        sys.exit()

    def run(self) -> None:
        is_running = True
        while is_running:
            self.actions[FrameEvent.FRAME_STARTED]()
            for event in pygame.event.get():
                if event.type == QUIT:
                    self.exit()
                elif event.type == KEYDOWN:
                    key_event = None
                    if pygame.key.get_pressed()[K_UP]:
                        key_event = KeyEvent.UP
                    elif pygame.key.get_pressed()[K_DOWN]:
                        key_event = KeyEvent.DOWN
                    elif pygame.key.get_pressed()[K_LEFT]:
                        key_event = KeyEvent.LEFT
                    elif pygame.key.get_pressed()[K_RIGHT]:
                        key_event = KeyEvent.RIGHT
                    elif pygame.key.get_pressed()[K_ESCAPE]:
                        key_event = KeyEvent.ESCAPE
                    elif pygame.key.get_pressed()[K_RETURN]:
                        key_event = KeyEvent.ENTER
                    elif pygame.key.get_pressed()[K_SPACE]:
                        key_event = KeyEvent.SPACE

                    should_finish = self.actions[key_event]()
                    is_running = is_running or not should_finish
            self.actions[FrameEvent.FRAME_ENDED]()
