from event_handling.event_loop import EventLoop
from event_handling.key_event import KeyEvent
from game.game import Game
from rendering.canvas import Alignment, Canvas


class MainMenu:
    def __init__(self):
        width = 400
        height = 400

        self.option = 0

        self.canvas = Canvas(width, height)
        self.event_loop = EventLoop()

        self.draw_menu()
        self.run()

    def __move_up(self) -> bool:
        self.option -= 1
        if self.option < 0:
            self.option = 1
        self.draw_menu()
        return False

    def __move_down(self) -> bool:
        self.option += 1
        if self.option > 1:
            self.option = 0
        self.draw_menu()
        return False

    def __select_option(self) -> bool:
        if self.option == 0:
            Game()
        elif self.option == 1:
            self.event_loop.exit()
        return True

    def draw_menu(self):
        self.canvas.clear()

        self.canvas.draw_text(
            "Play", (0, -25), Alignment.CENTER, Alignment.CENTER, self.option == 0
        )
        self.canvas.draw_text("Quit", (0, 25), Alignment.CENTER, Alignment.CENTER, self.option == 1)

        self.canvas.blit()

    def run(self):
        self.event_loop.register_action(KeyEvent.UP, self.__move_up)
        self.event_loop.register_action(KeyEvent.DOWN, self.__move_down)
        self.event_loop.register_action(KeyEvent.ESCAPE, self.event_loop.exit)
        self.event_loop.register_action(KeyEvent.ENTER, self.__select_option)

        self.event_loop.run()
