from enum import Enum
from typing import Dict, Tuple

import pygame

from config.app_config import APP_CONFIG
from rendering.colors import Color

Rect = Tuple[int, int, int, int]
Pos = Tuple[int, int]


class Alignment(Enum):
    LEFT = 0
    RIGHT = 1
    CENTER = 2
    TOP = 3
    BOTTOM = 4


class Canvas:
    def __init__(self, width: int, height: int):
        pygame.init()
        pygame.display.set_caption("Danger noodle")

        self.font = pygame.font.SysFont("Arial", 25)

        self.width = width
        self.height = height
        self.clear_color = APP_CONFIG.get_background_color()
        self.text_color = APP_CONFIG.get_text_color()

        self.text_cache: Dict[str, pygame.Surface] = {}

        self.canvas = pygame.display.set_mode((width, height))
        self.clear()

    def clear(self) -> None:
        self.canvas.fill(self.clear_color)

    def blit(self) -> None:
        pygame.display.flip()

    def draw_rect(self, rect: Rect, color: Color) -> None:
        pygame.draw.rect(self.canvas, color, rect, 0)

    def __get_text_block(self, text: str):
        if text in self.text_cache:
            return self.text_cache[text]
        else:
            text_block = self.font.render(text, True, self.text_color)
            self.text_cache[text] = text_block
            return text_block

    def draw_text(
        self,
        text: str,
        pos: Pos,
        hor_alignment: Alignment,
        ver_alignment: Alignment,
        hasFrame: bool = False,
    ) -> None:
        text_block = self.__get_text_block(text)
        padding = 4
        full_padding = padding * 2

        x_pos = pos[0] + padding
        y_pos = pos[1] + padding

        if hor_alignment == Alignment.RIGHT:
            x_pos = self.width - text_block.get_width() - pos[0] - padding
        elif hor_alignment == Alignment.CENTER:
            x_pos = (self.width / 2) - (text_block.get_width() / 2) + pos[0] + padding

        if ver_alignment == Alignment.BOTTOM:
            y_pos = self.height - text_block.get_height() - pos[1] - padding
        elif ver_alignment == Alignment.CENTER:
            y_pos = (self.height / 2) - (text_block.get_height() / 2) + pos[1] + padding

        rect = (
            x_pos - padding,
            y_pos - padding,
            text_block.get_width() + full_padding,
            text_block.get_height() + full_padding,
        )
        pygame.draw.rect(self.canvas, self.clear_color, rect, 0)
        self.canvas.blit(text_block, (x_pos, y_pos))
        if hasFrame:
            rect = (
                x_pos - padding,
                y_pos - padding,
                text_block.get_width() + full_padding,
                text_block.get_height() + full_padding,
            )
            pygame.draw.rect(self.canvas, self.text_color, rect, 2)
