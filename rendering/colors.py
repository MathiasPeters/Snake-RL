from enum import Enum
from typing import Tuple

Color = Tuple[int, int, int]


class ColorMode(Enum):
    DARK = 0
    LIGHT = 1


WHITE = (255, 255, 255)
BLACK = (0, 0, 0)


def rgb(r: int, g: int, b: int) -> Color:
    return (r, g, b)
